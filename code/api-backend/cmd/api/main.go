package main

import (
    "fmt"
    "log"
    "net/http"
)

const port = 8080

type application struct {
     Domain string
}

func main() {
     // set aplication config
     var app application

     // read from the command line

     // connect to the database

     app.Domain = "example.com"

     log.Print("Starting application on port ", port)

     // start a web server
     err := http.ListenAndServe(fmt.Sprintf(":%d", port), app.routes())
     if err != nil {
     	log.Fatal(err)
     }
     
}