# Instalación de Go y primeros pasos

## Introducción

Hola, voy a ir anotando lo que crea que es importante para ir aprendiendo a programar en Go. Voy a hacer sesiones que duren cierta cantidad de tiempo, el que voy a ir midiendo con videos de música de youtube. Para esta ocasión voy a usar [este](https://www.youtube.com/watch?v=qXC4AyjRikg).

Las herramientas que estoy usando son bien básicas. Estoy usando Linux, Ubuntu 20.04 especificamente. Las notas y el código están siendo escritas usando EMACS. No tengo grandes personalizaciones de las herramientas que uso porque soy una persona muy dispersa. Prefiero mantener al mínimo las situaciones de distracción.

Me motiva dejar este registro por un par de razones. Creo que cuando voy escribiendo lo que aprendo, logro retenerlo mejor y además me gusta ir dejando todo en un lugar al que puead volver para revisarlo. Además quiero generar contenido para ir publicando en mi página web que tengo abandonada. También existe un deseo de ir sistematizando mi aprendizaje para tener más confianza en el ambito profesional.

Ya mucho bla, vamo al codigo.

## Instalación de Go

La instalación en linux es bastante simple. Consta de 3 pasos.

1.- Eliminar cualquier otra instalación antigua del programa. Para eso eliminar todos los archivos del directorio llamado go en la carpeta del usuario local (/usr/local).
2.- Descargar el archivo comprimido desde la página oficial de descarga [go.dev/doc/install](go.dev/doc/install).
3.- Descomprimir el archivo en la carpeta local del usuario. La misma del punto 1.

Luego puedes verificar que todo funcionó correctamente con el comando go version.

Personalmente no tuve ningún problema con la instalción.

## Acerca de go y porque ese lenguaje

Mi carrera como desarrollador siempre se ha enfocado principalmente a la resolución de problemas. Con ese objetivo en mente, creo importante tener a la mano herramientas variadas que permitan solcionar diferentes problemas. Es una práctica común adaptar la elección de la herramienta para el tipo de problema que se necesita resolver.

Si bien go es un lenguaje de codigo abierto, está mantenido por Google. Una empresa que pasé de admirar a odiar. Independiente de las opniones personales que pueda tener al respecto, es una empresa que aporta significativamente al mundo de la informática y que tiene un tan grande poder, he ahí uno de mis reparos, que se puede dar el lujo de tener un equipo dedicado a sustentar la creación de un lenguaje de programción nuevo.

Personalmente elegí este lenguaje puesto que con un grupo de amigues estamos, ya hace varios años, desarollando una solución para digitalizar ciertos procesos en una cafetería. Hemos pasado por varias etapas y cada vez que volvemos a la tarea cambiamos el stack de desarollo. Es un desarrollo que nos ha permitido aprender un montón, por eso nos damos esas licencias.

Me gustaría hacer una tabla de comparación de los lenguajes que manejo y sus principales características. Pero ese no es el foco de estas letras.

Una cuestión que me faltó decir es que mi tarea principal en este desarollo recae en escribor las migraciones para la construcción de la base de datos del sistema. Ese es el fin, pero conozco tan poco de go que tengo que partir de las bases.

## Notas capítulo 1

Hemos decidido crear nuestro proyecto como una plataforma web, por lo que el foco del aprendizaje del len
guaje es el backend de la plataforma. Digo esto pues, hay ciertos estandares que se usan en ese ambiente 
de trabajo es bueno respetar.

Resulta ser que los programas en go están separados cada uno como un módulo. Con esto queiro decir que si
 construyo una herramienta específica para solucionar un problema particular lo que hago en go es desarro
llar un módulo. Para iniciar un nuevo modulo, uso el comando go mod init <moduleName>.

Luego, por convención, la fuente principal de mi programa estaŕa dentro de la carpeta api que a su vez es
tá dentro de la carpeta cmd.

Resulta	ser que	cada archivo que creo debe pertencer a un paquete en específico. En el caso de nuestro ar
chivo inicial, pertenece al paquete `main`.


En go, cuando creamos un servicio web, resulta ser que a diferencia de otros lenguajes viene pensado para ser usado en producción.

TAREA: Revisar las librerias http y fmt de go para ver cuales son los métodos que tienen y que hace.

## Notas capítulo 2

Usaremos un manejador de un tercero.

Cada programa de go tienen una y solo una función llamada main, que es el punto de entrada para nuestra aplicación.

En esta sesión utilizamos el multiplexor que viene en el paquete http de las librerias estandares de go para mostrar un mensaje cuando llamamos a la ruta raiz en el navegador.

## Notas capítulo 3

Reemplazamos el multiplexor de la sesión anterior por uno mejor llamado chi.

## Notas capítulo 4

En base a este nuevo multiplexor de terceros, estamos creando nuestro propio multiplexor en el archivo routes.go.

Permitimos que la función del router tenga acceso a los elementos de la aplicación y agregamos la ruta raiz del proyecto para que muestre un mensaje.